let btnMenu = document.querySelector('.navbar__burger');
let menu = document.querySelector('.navbar__list');

btnMenu.addEventListener('click', (event)=>{
    
    toggleMenu();
    event.stopPropagation();
});

function toggleMenu() { 
    menu.classList.toggle('visible'); 
    btnMenu.classList.toggle('open');
};


menu.addEventListener('click', (event) => {
    event.stopPropagation();
});

let titles = document.querySelectorAll('.navbar__list-item');

for (let i = 0; i < titles.length; i++) {
    titles[i].addEventListener('click', (event) => {

        let oldActive = document.querySelector('.navbar__list-item.active');
        oldActive.classList.remove('active');
        event.target.classList.add('active');
    });
};
